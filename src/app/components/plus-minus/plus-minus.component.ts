import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-plus-minus',
  templateUrl: './plus-minus.component.html',
  styleUrls: ['./plus-minus.component.css']
})
export class PlusMinusComponent implements OnInit {

  public name = 'Scloud';
  public successClass = 'text-success';
  public isError = false;
  public isSpecial = true;
  public messageClasses = {
    'text-success': !this.isError,
    'text-danger': this.isError,
    'text-special': this.isSpecial
  };

  constructor() { }

  ngOnInit() {
  }
}
